package chat;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class Controller {

    @FXML
    TextArea textArea;

    @FXML
    TextField textField;

    private static int countMessages = 0;

    public void sendMessage() {
        String message = textField.getText().trim();
        if (message.isEmpty()) return;
        countMessages++;
        textArea.appendText("Message #" + countMessages + ": " + message + "\n");
        textField.clear();
        textField.requestFocus();
    }

    // Отправка сообщений по CTRL + ENTER
    public void textFieldOnKeyPressed(KeyEvent e) {
        if (e.getCode() == KeyCode.ENTER && e.isControlDown()) {
            sendMessage();
        }
    }
}
